﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Ssepan.Utility.Core
{
    public class Lookup
    {
        public Lookup()
        {
        }

        public Lookup
        (
            string value,
            string text
        ) :
            this()
        {
            Value = value;
            Text = text;
        }
		public string Value { get; set; }
		public string Text { get; set; }

		public static List<Lookup> GetEnumLookup<TEnum>(bool addNotSelectedItem = false)
        {
            List<Lookup> returnValue = default;

            try
            {
                returnValue = (from Enum value in Enum.GetValues(typeof(TEnum))
                               select new Lookup(value.ToString(), value.ToString())).ToList();
                if (addNotSelectedItem)
                {
                    returnValue.Insert(0, new Lookup(string.Empty, "(Not Selected)"));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
    }
}
