﻿
using System;
using System.Diagnostics;
using System.Reflection;

namespace Ssepan.Utility.Core
{
    public static class Log
    {
        //Replacement for System.Diagnostic.EventLogEntryType enum
        public const string EventLogEntryType_Error="Error";
        public const string EventLogEntryType_Warning = "Warning";
        public const string EventLogEntryType_Information = "Information";

        /// <summary>
        /// Write log entry.
        /// General-purpose version that can be used for any situation.
        /// </summary>
        /// <param name="message">string</param>
        /// <param name="type">string</param>
        public static void Write
        (
            string message,
            string type = EventLogEntryType_Error
        )
        {
            Console.Error.WriteLine(string.Format("{0}\t{1}\t{2}", DateTime.Now, type, message));
        }

        /// <summary>
        /// Writes a message to the Application event log.
        /// Special-purpose version that is designed for use with exceptions.
        /// exception: an exception that we want to write to log
        /// ex: an exception that may occur when we try to write exception to log
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <param name="currentMethod">MethodBase</param>
        /// <param name="entryType">string</param>
        public static void Write
        (
            Exception exception,
            MethodBase currentMethod,
            string entryType
        )
        {
            try
            {
				Write
				(
					FormatEntry(Build(exception, currentMethod), currentMethod.DeclaringType.FullName, currentMethod.Name),
                    entryType
                );
            }
            catch (Exception ex)
            {
                //this will appear in the UI
                throw new Exception(string.Format("Unable to write to log. \n Reason: {0} \n Message: {1}", ex.Message, exception.Message), exception);
            }
        }

        /// <summary>
        /// Formats entry header, using explicitly passed values.
        /// </summary>
        /// <param name="message">string</param>
        /// <param name="className">string</param>
        /// <param name="methodName">string</param>
        /// <returns>string</returns>
        public static string FormatEntry(string message, string className, string methodName)
        {
			string returnValue;
			try
            {
                //FormatEntry entry header
                returnValue = string.Format("Location: Logged\nClass: {0}\nMember: {1}\n\n", className, methodName);

                //Append message
                returnValue += message;

                //truncate to 1st 32K characters
                returnValue = returnValue[..Math.Min(returnValue.Length, 32000)];
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to format entry header.", ex);
            }

            return returnValue;
        }

        /// <summary>
        /// Formats entry for layout of nested messages, using explicitly passed values.
        /// </summary>
        /// <param name="message">string</param>
        /// <returns>string</returns>
        public static string FormatEntryDetail(string message)
        {
			string returnValue;
			try
            {
                //Format entry detail
                returnValue = string.Format("{0}\n\n", message);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to format entry detail.", ex);
            }

            return returnValue;
        }

        /// <summary>
        /// Formats message with detail information describing the error, using explicitly passed values.
        /// </summary>
        /// <param name="message">string</param>
        /// <param name="stackFrameInfo">string</param>
        /// <param name="innerMessage">string</param>
        /// <returns>string</returns>
        public static string FormatMessage(string message, string stackFrameInfo, string innerMessage)
        {
			string returnValue;
			try
            {
                //Format entry detail
                returnValue = string.Format("[{0}Message: {1}\n\n{2}\n]\n", stackFrameInfo, message, innerMessage);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to format message.", ex);
            }

            return returnValue;
        }

        /// <summary>
        /// Builds message from passed exception and any inner-exceptions.
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <param name="currentMethod">MethodBase</param>
        /// <returns>string</returns>
        public static string Build(Exception exception, MethodBase currentMethod)
        {
			string exceptionMessage = string.Empty;
			string innerExceptionMessage = string.Empty;

			string returnValue;
			try
			{
				if (exception != null)
				{
					if (exception.InnerException != null)
					{
						//Build inner message.
						innerExceptionMessage = Build(exception.InnerException, currentMethod);
					}

					//Build outer message
					exceptionMessage = FormatMessage(exception.Message, GetStackFrameInfo(exception), innerExceptionMessage);
				}

				returnValue = exceptionMessage;
			}
			catch (Exception ex)
			{
				throw new Exception("Unable to build message.", ex);
			}

			return returnValue;
        }

        /// <summary>
        /// Gets line number, method, and class of error from stack trace.
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <returns>string</returns>
        public static string GetStackFrameInfo(Exception exception)
        {
            string returnValue = string.Empty;
			StackFrame[] stackFrames;
			try
			{
				if (exception.StackTrace == null)
				{
					returnValue += string.Format("\n Location: {0} \n Class: {1} \n Member: {2} \n Line: {3} \n ", "n/a", "n/a", "n/a", "n/a");
				}
				else
				{
					StackTrace stackTrace = new(exception, true);

					stackFrames = stackTrace.GetFrames();
					// Console.WriteLine(string.Format("stackFrames.Length:{0}",  stackFrames.Length.ToString()));
					// Console.WriteLine(string.Format("stackFrames.GetLowerBound(0):{0}" , stackFrames.GetLowerBound(0).ToString()));
					// Console.WriteLine(string.Format("stackFrames.GetUpperBound(0):{0}" , stackFrames.GetUpperBound(0).ToString()));
					for (int i = stackFrames.GetLowerBound(0); i <= stackFrames.GetUpperBound(0); i++)
					{
						string location = i switch
						{
							0 => "Thrown",
							1 => "Caught",
							_ => "n/a",
						};
						StackFrame stackFrame = stackFrames[i];
						// Console.WriteLine(string.Format("stackFrames[{0}]:{1}" , i.ToString(), stackFrames[i]));

						string lineNumber = stackFrame.GetFileLineNumber().ToString();
						string methodName;
						string className;
						// Console.WriteLine(string.Format("lineNumber:{0}" , lineNumber.ToString()));
						if (stackFrame.GetMethod() == null)
						{
							// Console.WriteLine(string.Format("stackFrame.GetMethod():{0}" , "is null"));
							methodName = "n/a";
							className = "n/a";
						}
						else
						{
							methodName = stackFrame.GetMethod().Name;
							// Console.WriteLine(string.Format("methodName:{0}" , methodName.ToString()));
							if (stackFrame.GetMethod().DeclaringType == null)
							{
								className = "n/a";
							}
							else
							{
								className = stackFrame.GetMethod().DeclaringType.FullName;
							}
						}

						returnValue += string.Format("\nLocation: {0}\nClass: {1}\nMember: {2}\nLine: {3}\n", location, className, methodName, lineNumber);
					}
				}
			}
			catch (Exception ex)
			{
				throw new Exception("Unable to get error line info.", ex);
			}

			return returnValue;
        }
   }
}
