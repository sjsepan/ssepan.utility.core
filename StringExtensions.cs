﻿using System;

namespace Ssepan.Utility.Core
{
    public static class StringExtensions
    {
        /// <summary>
        /// Perform string.Contains(value) with case-sensitivity explicitly on or off.
        /// </summary>
        /// <param name="source">this string</param>
        /// <param name="value">string</param>
        /// <param name="caseInsensitive">bool</param>
        /// <returns>bool</returns>
        public static bool Contains(this string source, string value, bool caseInsensitive)
        {
            if (caseInsensitive)
            {
                int results = source.IndexOf(value, StringComparison.CurrentCultureIgnoreCase);
                return results != -1;
            }
            else
            {
                return source.Contains(value);
            }
        }
    }
}
