﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Ssepan.Utility.Core
{
    /// <summary>
    /// http://www.devexpertise.com/category/net/linq/
    /// </summary>
    public static class LikeOperator
    {
        /// <summary>
        /// Ex: var results = (from v in values where v.Like("*a*a*") select v);
        /// </summary>
        /// <param name="value">this string</param>
        /// <param name="term">string</param>
        /// <returns>bool</returns>
        public static bool Like(this string value, string term)
        {
			bool returnValue;
			try
			{
				Regex regex = new(string.Format("^{0}$", term.Replace("*", ".*")), RegexOptions.IgnoreCase);
				returnValue = regex.IsMatch(value ?? string.Empty);
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				throw;
			}
			return returnValue;
        }

        /// <summary>
        /// Ex: var results = values.Like("*a*a*");
        /// </summary>
        /// <param name="source">this IEnumerable<string></param>
        /// <param name="expression">string</param>
        /// <returns>IEnumerable<string></returns>
        public static IEnumerable<string> Like(this IEnumerable<string> source, string expression)
        {
            IEnumerable<string> returnValue = default;

            try
            {
                returnValue = (from s in source where s.Like(expression) select s);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
            return returnValue;
        }
    }
}
