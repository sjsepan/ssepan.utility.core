﻿using System;
using System.Reflection;

namespace Ssepan.Utility.Core
{
    /// <summary>
    /// Posted by user 'ILoveFortran' at http://stackoverflow.com/questions/476589/how-do-i-get-from-a-type-to-the-tryparse-method on stackoverflow.com
    /// </summary>
    public static class Parsing
    {
        static MethodInfo FindTryParseMethod(Type type)
        {
            //find member of type with signature 'static public bool TryParse(string, out TStruct)'
            const BindingFlags access = BindingFlags.Static | BindingFlags.Public;
            MemberInfo[] candidates = type.FindMembers(
                MemberTypes.Method,
                access,
				(MemberInfo m, object _) =>
				{
					MethodInfo method = (MethodInfo)m;
					if (method.Name != "TryParse") return false;
					if (method.ReturnParameter.ParameterType != typeof(bool)) return false;
					ParameterInfo[] parameters = method.GetParameters();
					if (parameters.Length != 2) return false;
					if (parameters[0].ParameterType != typeof(string)) return false;
					if (parameters[1].ParameterType != type.MakeByRefType()) return false;
					if (!parameters[1].IsOut) return false;

					return true;
				}, null);

            if (candidates.Length > 1)
            {
                //change this to your favorite exception or use an assertion
                throw new Exception(string.Format(
                    "Found more than one method with signature 'public static bool TryParse(string, out {0})' in type {0}.",
                    type));
            }
            if (candidates.Length == 0)
            {
                //This type does not contain a TryParse method - replace this by your error handling of choice
                throw new Exception(string.Format(
                    "Found no method with signature 'public static bool TryParse(string, out {0})' in type {0}.",
                    type));
            }
            return (MethodInfo)candidates[0];
        }

        public static bool TryParse(Type t, string s, out object val)
        {
            MethodInfo method = FindTryParseMethod(t); //can also cache 'method' in a Dictionary<Type, MethodInfo> if desired
            object[] oArgs = [s, null];
            bool bRes = (bool)method.Invoke(null, oArgs);
            val = oArgs[1];
            return bRes;
        }

        //if you want to use TryParse in a generic syntax:
        public static bool TryParseGeneric<T>(string s, out T val)
        {
			bool bRes = TryParse(typeof(T), s, out object oVal);
			val = (T)oVal;
            return bRes;
        }
    }
}
